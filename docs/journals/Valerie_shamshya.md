## 06/26/23:
* Started our work with authentication following video instructions

## 06/27/23
* Worked on authentication as a group
* Finished authentication
* Added models

## 06/28/23
* Started to create a route to get a list of all video game platfroms

## 06/29/23
* Finished work on the video game platforms routes and queries

## 07/16/23
* Started work on unit tests

## 07/17/23
* Continued work on unit tests
* Finished unit tests for favorites
* Finished unit tests for platforms
* Worked on getting the unit test for video games working, wasn't able to get them working, decided with the group to skip those for now.
* Re-worked all routes and queries files sligthly to add dependency injection

## 07/24/23
* Re-installed the project on my computer, ended up having problems with the setup and spent most of the day trying to fix it

## 07/25/23
* Worked out the setup issues with SEIRs
* Fixed link to game details page from game list page
* Started on getting the search function working

## 07/26/23
* Finished the search function for list pages
* implemented pagination for game list and platform list pages
* worked with the group to figure out login

## 07/27/23
* Spend the first half of the day trying to figure out login issue
* Worked with instructors after lunch which helped us get the login to work
* worked through merging and updating code in main branch
* implemented the correct logout
