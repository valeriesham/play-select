# Wire-frame diagrams

![Play Select Diagram](/docs/Images/diagram.png "Diagram")
![Play Select- Main page](docs/../Images/MainPage.png "Main-Page")
![Play Select- Login Page](docs/../Images/Login.png "login")
![Play Select- Signup Page](docs/../Images/Signup.png "SignUp")
![Play Select-Profile Page](docs/../Images/ProfilePage.png "Profile Page")
![Play Select-Game Page](docs/../Images/GamePage.png "Game Page")
![Play Select-Game Detail Page](docs/../Images/GameDetail.png "Game Detail Page]")
