import React, { useEffect, useState } from "react";
import "../Styles/Forms.css"
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { AiFillEyeInvisible, AiFillEye } from "react-icons/ai";
import { Link } from "react-router-dom";
import { useSignupMutation } from "../Store/apiSlice";

const SignUp = () => {
  const navigate = useNavigate();
  const [signup, signupResult] = useSignupMutation();
  const [errorMessage, setErrorMessage] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [email, setEmail] = useState('');

  const [passwordShow, setPasswordShow] = useState(false);
  const [passwordConfirmationShow, setPasswordConfirmationShow] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (password !== passwordConfirmation) {
      setErrorMessage("Passwords do not match, please try again");
      return;
    };

    const formData = {
      email,
      password,
      first_name: firstName,
      last_name: lastName,
      username,
    };

    try {
      const response = await signup(formData);
      if (response.error) {
        if (response.data && response.data.detail) {
          setErrorMessage(response.data.detail);
        } else {
          setErrorMessage("Signup failed. Please check your data and try again.");
        }
      } else {
        toast.success(`Welcome, ${username}`, {
          position: "bottom-right",
          autoClose: 4000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        navigate("/");
      }
    } catch (error) {
      console.error("Error during signup:", error);
      setErrorMessage("Signup failed. Please try again later.");
    }
  };

  const toggleShowPassword = () => {
    setPasswordShow(!passwordShow);
  };

  const toggleShowPasswordConfirmation = () => {
    setPasswordConfirmationShow(!passwordConfirmationShow);
  };

  useEffect(() => {
    if (signupResult.isSuccess) {
    }
  }, [signupResult.isSuccess]);

  return (
    <div className="row">
      <div className="col-md-6 offset-md-3">
          <h1> Please Sign Up!</h1>
          <form onSubmit={handleSubmit} id="create-account-form" className="form">
            {errorMessage &&
              (<div className="alert alert-danger" role="alert">
                {errorMessage}
              </div>
              )}
            <div className="mb-3">
            First Name <input
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="first_name_input"
                className="first_nameForm"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
              <label htmlFor="first_name_input"></label>
            </div>
            <div className=" mb-3">
              Last Name <input
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="last_name_input"
                className="last_nameForm"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
              <label htmlFor="last_name_input"></label>
            </div>
            <div className="mb-3">
              Username <input
                placeholder="Username"
                required
                type="text"
                name="Username"
                id="Usernameinput"
                className="UsernameForm"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
              <label htmlFor="Usernameinput"></label>
            </div>
            <div className="mb-3">
              Password <input
                placeholder="Password"
                required
                type={passwordShow ? "text" : "password"}
                name="Password"
                id="Passwordinput"
                className="PasswordForm"
                onChange={(e) => {
                  setPassword(e.target.value)
                  setErrorMessage('')
                }}
              />{passwordShow ? (
                <AiFillEyeInvisible onClick={toggleShowPassword} />
              ) : (
                <AiFillEye onClick={toggleShowPassword} />
              )}
              <label htmlFor="Passwordinput"></label>
            </div>
            <div className="mb-3">
              Password Confirmation <input
                placeholder="Password Confirmation"
                required
                type={passwordConfirmationShow ? "text" : "password"}
                name="PasswordConfirmation"
                id="PasswordConfirmationInput"
                className="PasswordConfirmationForm"
                onChange={(e) => {
                  setPasswordConfirmation(e.target.value)
                  setErrorMessage('')
                }}
              />{passwordConfirmationShow ? (
                <AiFillEyeInvisible onClick={toggleShowPasswordConfirmation} />
              ) : (
                <AiFillEye onClick={toggleShowPasswordConfirmation} />
              )}
              <label htmlFor="PasswordConfirmationInput">
              </label>
            </div>
            <div className="mb-3">
              Email <input
                placeholder="Email"
                required
                type="email"
                name="Email"
                id="Email"
                className="EmailForm"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <label htmlFor="Email"></label>
            </div>
            <div className="text-sm text-gray-500 mb-5">
              Already have an account?{" "}
              <Link
                className="SignInLink"
                to="/login"
                onClick={(e) => {
                  e.preventDefault();
                  navigate("/login");
                }}
              >
                Login here
              </Link>
            </div>
            <button type="submit" className="original-button">
              Sign Up
            </button>
          </form>
        </div>
      </div>
  );
}

export default SignUp;
