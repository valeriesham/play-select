import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import { gameApi } from './apiSlice';
import searchReducer from './searchSlice';

export const store = configureStore({
    reducer: {
        [gameApi.reducerPath]: gameApi.reducer,
        search: searchReducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware()
            .concat(gameApi.middleware),
});

setupListeners(store.dispatch);

export default store;
