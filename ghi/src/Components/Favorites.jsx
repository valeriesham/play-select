import React from "react";
import { useGetFavoritesQuery } from "../Store/apiSlice";

const Favorites = () => {
    const { data, isLoading, isError, error } = useGetFavoritesQuery();

    if (isLoading) return <div>Loading...</div>;

    if (isError) return <div>Error: {error.message}</div>;

    if (!data || !data.favorites || data.favorites.length === 0) {
        return <div>No favorite games found.</div>;
    }

    return (
        <div>
            <h2>My Favorite Games</h2>
            {data.favorites.map((favorite) => (
                <div key={favorite.id}>
                    <h3>Game Name: {favorite.game_name}</h3>
                </div>
            ))}
        </div>
    );
};

export default Favorites;
