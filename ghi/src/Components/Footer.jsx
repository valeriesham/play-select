import React from 'react';
import RAWGlogo from '../images/RAWGlogo.png';
import { Link } from 'react-router-dom';
import '../Styles/Footer.css';

const Footer = () => {
  return (
    <footer className="footer">
      <span className="footer-text">
        © 2023{" "}
        <Link to="/" className="footer-link">
          PlaySelect™
        </Link>
        . All Rights Reserved.
      </span>
      <ul className="footer-links">
        <li>
          <a
            href="https://gitlab.com/team-10_hrs_worst_nightmare/play-select"
            target="_blank"
            className="footer-link"
            rel="noopener noreferrer"
          >
            GitLab
          </a>
        </li>
        <li>
          <Link to="/creators" className="footer-link">
            Creators
          </Link>
        </li>
        <li>
          <a
            href="https://rawg.io/apidocs"
            target="_blank"
            className="footer-link"
            rel="noopener noreferrer"
          >
            <img
              className="footer-logo"
              id="RAWGlogo"
              width="40"
              height="auto"
              src={RAWGlogo}
              alt="rawg"
            />
          </a>
        </li>
      </ul>
    </footer>
  );
};

export default Footer;
