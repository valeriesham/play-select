import React from 'react';
import { useGetGenreQuery } from '../Store/apiSlice';
import { useSelector } from 'react-redux';

const GenresList = () => {
    const searchCriteria = useSelector((state) => state.search.value);
    const { data, isLoading } = useGetGenreQuery();

    if (isLoading) {
        return <div>Loading...</div>;
    }

    const filteredGenres = () => {
        if (searchCriteria) {
            const searchLowercase = searchCriteria.toLowerCase();
            return data.filter((genre) =>
                genre.name.toLowerCase().includes(searchLowercase)
            );
        } else {
            return data;
        }
    };

    return (
        <div className="mt-3">
            <h1>
                Genres
                <small id="seach-criteria"> {searchCriteria}</small>
            </h1>
            <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 mt-4">
                {filteredGenres().map((genre) => (
                    <div key={genre.id} className="col-sm-6 mb-3 mb-sm-0">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">{genre.name}</h5>
                                <p className="card-text">Game Count: {genre.games_count}</p>
                                <img src={genre.image_background} alt={genre.name} className="card-img-top" />
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default GenresList;
