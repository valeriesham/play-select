import { useNavigate } from 'react-router-dom';

function Construct({ info, isLoggedIn }) {
  const navigate = useNavigate();

  if (!isLoggedIn) {
    navigate('/login');
    return null;
  }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Under construction</h1>
        <h2>Coming on (07/29/2023)</h2>
        <h2>
          Module: 3 {info.module} Week: 17 {info.week} Day: 6 {info.day}
        </h2>
        <h2>
          by or <strong>WELL BEFORE</strong> {info.hour}:{info.min}{"07:00 "}
          Cohort Time
        </h2>
      </header>
    </div>
  );
}

export default Construct;
