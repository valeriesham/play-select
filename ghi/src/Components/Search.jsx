import { useState } from "react";
import { filter } from "../Store/searchSlice";
import { useDispatch } from "react-redux";

const Search = () => {
    const [searchCriteria, setSearchCriteria] = useState('');
    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('handleSubmit', searchCriteria);
        dispatch(filter(searchCriteria));
        setSearchCriteria('');
    }

    return (
        <form className="row" onSubmit={handleSubmit}>
            <div className="col">
                <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Search"
                    value={searchCriteria}
                    onChange={(e) => setSearchCriteria(e.target.value)}
                />
            </div>
            <div className="col">
                <button className="original-button" type="submit">Search</button>
            </div>
        </form>
    )
}

export default Search
