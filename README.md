# *PlaySelect*

![logo](ghi/src/images/PlaySelect.png)

## Here is a Sneak peak at our site

![Alt Text](ghi/src/Styles/video/playselect.gif)

![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodblogoColor=white) ![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB) ![Redux](https://img.shields.io/badge/redux-%23593d88.svg?style=for-the-badge&logo=redux&logoColor=white) ![Bootstrap](https://img.shields.io/badge/bootstrap-%238511FA.svg?style=for-the-badge&logo=bootstrap&logoColor=white)![FastAPI](https://img.shields.io/badge/FastAPI-005571?style=for-the-badge&logo=fastapi) ![SASS](https://img.shields.io/badge/SASS-hotpink.svg?style=for-the-badge&logo=SASS&logoColor=white) ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E) ![Markdown](https://img.shields.io/badge/markdown-%23000000.svg?style=for-the-badge&logo=markdown&logoColor=white) ![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white) ![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)  ![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

## *Created by*

* **Brandon Ramos**
* **Myron Atlas**
* **Valerie Shamshyna**
* **Jason Leisure**

The application offers an engaging platform for video game enthusiasts. Upon arriving on the main page, visitors are greeted warmly and are given the option to either register if they are new, or log in if they are returning users. Once logged in, users have the ability to delve into the details of individual video games, gaining an understanding of what each one has to offer. For personalization, users have the option to curate a list of favorite games, effectively tailoring their experience based on personal preference. Navigating the site is made user-friendly through a sophisticated navigation bar. It features dropdown links that allow users to filter game displays based on specific criteria, such as developer, publisher, platform, and genre, while a search function aids in finding specific games. Furthermore, the application offers a comprehensive user profile page, showcasing detailed user information. A unique feature is the "Favorites" page, designed using a React Redux API slice. This page efficiently generates a list of favored games from a MongoDB database, thus enhancing the personalization of the user experience.

---

## Design

* [API Design](docs/API_Design.md#api-documentation)

* [Wireframe Design](docs/Wireframe.md#wire-frame-diagrams)

## Play Select functionality

* The site's main page extends a warm welcome to its visitors, providing them the option to either register, in the case of first-time visitors, or log in if they are existing users.
* Users have the ability to peruse individual video games, wherein they can access in-depth details about each game.
* Users can curate a personalized selection of favorite games, providing them the ability to add individual games to their favorites list.
* The site's navigation bar features dropdown links that allow users to refine the display of games based on certain criteria, such as the developer, publisher, platform, and genre. In addition, it supports a search functionality to help users find specific games.
* The site includes a comprehensive user profile page that showcases user information. Additionally, it features a "Favorites" page which employs a React Redux API slice to generate a list from a MongoDB database.

## Set Up

How to setup on your local computer! 💻

[![Alt text](/docs/Images/gitlab.png)](<https://gitlab.com/team-10_hrs_worst_nightmare/play-select.git>)

Clone the Repo From GitLab

1. Open your terminal.
2. Create a new directory by typing the following command and replacing `directory_name` with the name you choose: `mkdir<< directory_name>>`
3. Navigate into the directory you just created by typing: `cd <<directory_name>>`
4. Clone the repository by typing the following command, : `git clone https://gitlab.com/team-10_hrs_worst_nightmare/play-select.git`

Make sure to replace `directory_name` with the actual directory name and repository link respectively.


[![Alt text](docs/Images/Docker.png)](<https://www.docker.com/>)

To set up and run the Docker environment, follow these steps:

1. Ensure that Docker is installed on your machine. If not, you can download and install it from the official Docker website. <https://www.docker.com/>

2. Open your terminal and navigate to the project directory. If you are not already inside the project directory, you can use `cd path_to_your_project_directory` to get there.

3. Once inside the project directory, create a Docker volume named "playselect" by running the following command: `docker volume create playselect`.

4. Build your Docker containers by using the Docker Compose command. Type the following in your terminal: `docker-compose build`.

5. Start your Docker containers with: `docker-compose up`.

Please note that building the Docker containers and images might take a while (around 5 minutes or more depending on your system's performance and internet speed).

Ensure you wait for the process to complete before proceeding with any other actions. You'll know that the process is complete when the terminal stops displaying new messages and returns to the command prompt.


[![Alt text](docs/Images/mongodb.png)](<https://www.mongodb.com/docs/>)

For this Web app we utilized MongoDB and MongoExpress

* Establish your database through your coice of Mongo/ Mongo-express, or PostGressSQL
  * If using Mongo DB
    * Access here: <http://localhost:8081/>
  * Create a collection
    * Create a new document and or Index
    * import any Mongo json file(optional)
  * This will create a database for you!

[![Alt text](docs/Images/FastAPI.png)](<https://fastapi.tiangolo.com/>)

For the backend we will be using FastAPI to make Queries to the 3rd party RAWG API Database

* see [API Design](docs/API_Design.md#api-documentation) for additional details

[![Alt text](docs/Images/react.png)](<https://react.dev/>)

Front End we Utilized React and React Redux

* This application utilzes React, Fast Api Redux and bootstrap
* Head over to the front end of the website, located on `localhost:3000` and enjoy using PlaySelect!
