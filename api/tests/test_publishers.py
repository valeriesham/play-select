from fastapi.testclient import TestClient
from main import app
from queries.publisher_queries import PublisherQueries

client = TestClient(app)


class FakePublisherQueries:

    def get_publishers(self):
        return [
            {
                "id": 354,
                "name": "Electronic Arts",
                "slug": "electronic-arts",
                "games_count": 1310,
                "image_background": "url"
            },
        ]


def test_get_publishers_list():
    app.dependency_overrides[PublisherQueries] = FakePublisherQueries
    res = client.get("/publishers")
    data = res.json()
    assert res.status_code == 200
    assert data == [
            {
                "id": 354,
                "name": "Electronic Arts",
                "slug": "electronic-arts",
                "games_count": 1310,
                "image_background": "url"
            },
        ]
