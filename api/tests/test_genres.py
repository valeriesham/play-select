from fastapi.testclient import TestClient
from main import app
from queries.genre_queries import GenreQueries

client = TestClient(app)


class FakeGenreQueries:
    def get_genres(self):
        return [
            {
                "id": 4,
                "name": "Action",
                "slug": "action",
                "games_count": 173502,
                "image_background": "url"
            }
        ]


def test_get_genre_list():
    app.dependency_overrides[GenreQueries] = FakeGenreQueries

    res = client.get("/genres")
    data = res.json()

    assert res.status_code == 200
    assert data == [
            {
                "id": 4,
                "name": "Action",
                "slug": "action",
                "games_count": 173502,
                "image_background": "url"
            }
        ]
