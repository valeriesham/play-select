from fastapi.testclient import TestClient
from main import app
from queries.platform_queries import PlatformQueries

client = TestClient(app)


class FakePlatformQueries:
    def get_platforms(self):
        return [
            {
                "id": 4,
                "name": "PC",
                "slug": "pc",
                "games_count": 514224,
                "image_background": "url",
                "image": "",
                "year_start": 0,
                "year_end": 0
            }
        ]


def test_get_platform_list():
    app.dependency_overrides[PlatformQueries] = FakePlatformQueries

    res = client.get("/platforms")
    data = res.json()

    assert res.status_code == 200
    assert data == [
            {
                "id": 4,
                "name": "PC",
                "slug": "pc",
                "games_count": 514224,
                "image_background": "url",
                "image": "",
                "year_start": 0,
                "year_end": 0
            }
        ]
