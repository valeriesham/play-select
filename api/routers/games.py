from fastapi import APIRouter, HTTPException, Query
from queries.game_queries import fetch_games, fetch_game_by_id
from models import Game

router = APIRouter()


@router.get("/games")
async def get_games(limit: int = Query(500), offset: int = Query(0)):
    try:
        games = await fetch_games()
        games = games[offset: offset + limit]
        return games
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/games/{game_id}", response_model=Game)
async def get_game_by_id(game_id: str):
    try:
        game = await fetch_game_by_id(game_id)

        if game is None:
            raise HTTPException(status_code=404, detail="Game not found")
        return game
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
