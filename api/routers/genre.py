from typing import List

from fastapi import APIRouter, Depends

from models import Genre
from queries.genre_queries import GenreQueries

router = APIRouter()


@router.get("/genres", response_model=List[Genre])
def get_genres(
    repo: GenreQueries = Depends()
):
    genres_list = repo.get_genres()
    return genres_list
